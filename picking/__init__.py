    
''' --- Class Registration --- '''
def register():    
    from . picking_ops import register as register_ops
    register_ops()

def unregister():     
    from . picking_ops import unregister as unregister_ops
    unregister_ops()

if __name__ == "__main__":
    register() 
