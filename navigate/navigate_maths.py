import bpy
from .. common.color import *
from mathutils import *
from bl_math import clamp

def drive_hsv(old_driving_color, new_driving_color, driven_colors):
    dh = new_driving_color.h - old_driving_color.h
    ds = new_driving_color.s - old_driving_color.s
    dv = new_driving_color.v - old_driving_color.v

    # print(f"Old color {old_driving_color}, hsv {old_driving_color.hsv}, hex {rgba2hex(old_driving_color)}")
    # print(f"New color {new_driving_color}, hsv {new_driving_color.hsv}, hex {rgba2hex(new_driving_color)}")
    # print("dHSV", dh, ds, dv)

    for color in driven_colors:
        color.h = (color.h + dh) % 1 
        color.s = clamp(color.s + ds, 0, 1)
        color.v = clamp(color.v + dv, 0, 1)

def drive_colors(old_driving_color, new_driving_color, driven_colors, mode):
    if mode == 'HSV':
        drive_hsv(old_driving_color, new_driving_color, driven_colors)
        return
    return

