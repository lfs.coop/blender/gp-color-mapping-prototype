    
''' --- Class Registration --- '''
def register():    
    from . navigate_ops import register as register_ops
    register_ops()

    from . navigate_panel import register as register_panel
    register_panel()

def unregister():     
    from . navigate_panel import unregister as unregister_panel
    unregister_panel()

    from . navigate_ops import unregister as unregister_ops
    unregister_ops()

if __name__ == "__main__":
    register() 
