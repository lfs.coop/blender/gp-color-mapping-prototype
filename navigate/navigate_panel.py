import bpy
from bpy.props import *
from bpy.types import UILayout

class GPCOLORMAP_UL_ColorExplorerList(bpy.types.UIList):
    bl_idname="GPCOLORMAP_UL_ColorExplorerList"

    def draw_item(self, context, layout, data, mapping, icon, active_data,
                  active_propname, index):  
        row = layout.row()
        row.ui_units_x = 2

        ''' Splitting the item layout in two columns '''
        cont = row.split(factor=0.3)

        ''' 1st column '''
        col = cont.column()

        ''' Let some space so that we can select the mapping '''
        row = col.row()
        row.separator(factor=3)

        is_driving_mapping = (data.active_index == index)
        locked_icon = 'LOCKED' if mapping.is_locked else 'UNLOCKED'
        row.prop(mapping, 'is_locked', icon=locked_icon, emboss = False, icon_only= True)
        row.enabled = not is_driving_mapping

        ''' 2nd column '''
        col = cont.column()
        row = col.row(align=True)
        if mapping.has_stroke() and (not mapping.is_linked):
            row.prop(mapping, 'stroke_color', icon_only=True)

        if mapping.has_fill():
            row.prop(mapping, 'fill_color', icon_only=True)

class GPCOLORMAP_MT_LockMenu(bpy.types.Menu):
    bl_idname = "GPCOLORMAP_MT_lock_menu"
    bl_label = "Locking operations"

    def draw(self, context):
        layout = self.layout
        
        cmap = context.scene.gpcolormapping

        layout.operator("scene.lock_mappings", icon="UNLOCKED", text="Unlock all").mode = 'UNLOCK'
        layout.operator("scene.lock_mappings", icon="LOCKED", text="Lock all").mode = 'LOCK'
        layout.operator("scene.lock_mappings", icon="ARROW_LEFTRIGHT", text="Toggle all").mode = 'TOGGLE'
        layout.separator()
        layout.prop(cmap, "lock_inactive_groups")

class GPCOLORMAP_PT_ColorExplorerPanel(bpy.types.Panel):
    bl_label="Color Explorer"
    bl_idname="GPCOLORMAP_PT_ColorExplorerPanel"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context="scene"
    bl_parent_id='GPCOLORMAP_PT_Manager'

    def draw(self, context):
        layout = self.layout
        
        '''-- Specific case of no active mapping in the list --'''
        cmap = context.scene.gpcolormapping
        active_mapping = cmap.active_mapping()
        if active_mapping is None:
            row = layout.row()
            row.label(text="No active mapping")
            return
        '''-----------------------------------------------------'''

        base_mat = active_mapping.base_material
        split_size = 0.4

        '''-- Splitting the layout in 2 columns --'''
        cont = layout.split(factor=split_size)

        ''' == 1st column == '''
        col = cont.column()

        ''' 1st row '''
        row = col.row()
        row.label(text=f'{base_mat.name}')
        row.prop_enum(cmap, "driving_color_mode", cmap.driving_color_mode, text='')

        col.separator()

        ''' 2nd row '''
        row = col.row()
        row.template_color_picker(cmap, 'driving_color', value_slider=True)

        ''' 3rd row '''
        col.separator()
        row = col.row()
        row.prop(cmap, 'driving_color', text='')

        ''' =============== '''

        ''' == 2nd column == '''
        col = cont.column()

        ''' 1st row '''
        row = col.row()
        row.alignment='RIGHT'
        row.prop(cmap, "driving_mode")
        # row.operator_menu_enum('scene.lock_mappings', 'mode', text='')
        row.menu("GPCOLORMAP_MT_lock_menu", text="")

        ''' 2nd row '''
        row = col.row()
        row.template_list("GPCOLORMAP_UL_ColorExplorerList",'GP_ColorMapping', \
                        dataptr=cmap, propname="mappings", \
                        active_dataptr=cmap, active_propname="active_index", \
                        type='GRID', columns=4
                        )
        ''' =============== '''
        '''-----------------------------------------------------'''

class GPCOLORMAP_PT_ColorExplorerViewportPanel(bpy.types.Panel):
    bl_label="Color Explorer"
    bl_idname="GPCOLORMAP_PT_ColorExplorerViewportPanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Color Mapping"

    def draw(self, context):
        gpcm = context.scene.gpcolormapping

        layout = self.layout

        row = layout.row()
        row.operator("scene.explore_palette_colors", text="Explore Color Palette", icon='COLOR')

        row = layout.row()
        row.prop(gpcm, "autogroup")

class GPCOLORMAP_UL_ColorExplorerDialogList(bpy.types.UIList): # find a more explicit name ?
    bl_idname="GPCOLORMAP_UL_ColorExplorerDialogList"

    def draw_item(self, context, layout, data, mapping, icon, active_data,
                  active_propname, index):
        self.use_filter_show = False

        row = layout.row()
        row.ui_units_y = 1

        col = row.column()
        col.scale_x = 1
        col.prop(mapping, "group", text='', emboss=False)

        row.separator()

        col = row.column()
        col.scale_x = 4
        row_colors = col.row()
        if mapping.has_stroke() and (not mapping.is_linked):
            row_colors.prop(mapping, 'stroke_color', icon_only=True)
        
        if mapping.has_fill():
            row_colors.prop(mapping, 'fill_color', icon_only=True)

        col = row.column()
        col.scale_x = 1
        icon_active = 'RESTRICT_RENDER_OFF' if mapping.is_active else 'RESTRICT_RENDER_ON'
        col.prop(mapping, "is_active", icon=icon_active, text='', emboss=False)

        col = row.column()
        col.scale_x = 1
        col.prop(mapping, "is_locked", icon='TRASH', text='', emboss=False)

    def filter_items(self, context, data, propname):
        mappings = getattr(data, propname)
        bitflag = self.bitflag_filter_item
        # Show unlocked items only
        filtered = [bitflag if not m.is_locked else bitflag & ~bitflag \
            for m in mappings]
        return filtered, []


classes = (GPCOLORMAP_UL_ColorExplorerList, \
           GPCOLORMAP_PT_ColorExplorerViewportPanel, \
           GPCOLORMAP_UL_ColorExplorerDialogList, \
           GPCOLORMAP_MT_LockMenu, \
           GPCOLORMAP_PT_ColorExplorerPanel)
def register():
    for cls in classes:
        bpy.utils.register_class(cls) 

def unregister():        
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)