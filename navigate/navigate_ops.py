import bpy
from bpy.props import *

class GPCOLORMAP_OT_lockMapping(bpy.types.Operator):
    bl_idname = "scene.lock_mappings"
    bl_label = "Lock Mappings"
    bl_description = "Lock mappings to prevent them for being globally changed"

    lock_modes = [('UNLOCK', "Unlock all", "Unlock all mappings", 'UNLOCKED', 1), \
                  ('LOCK', "Lock all", "Lock all mappings", 'LOCKED', 2),
                  ('TOGGLE', "Toggle all", "Toggle lock flag for all mappings", 'ARROW_LEFTRIGHT', 3)]
    mode: EnumProperty(items=lock_modes, default=1)

    @classmethod
    def poll(cls, context):
        return not context.scene.gpcolormapping.is_empty()

    def execute(self, context):   
        do_lock = (self.mode == 'LOCK')
        do_toggle = (self.mode == 'TOGGLE')

        cmap = context.scene.gpcolormapping
        active_mapping = cmap.active_mapping()

        for mapping in cmap.mappings:
            if mapping == active_mapping:
                continue

            mapping.is_locked = (not mapping.is_locked) \
                                if do_toggle else do_lock

        return {'FINISHED'}   

classes = (GPCOLORMAP_OT_lockMapping, )

def register():
    for cls in classes:
        bpy.utils.register_class(cls) 

def unregister():        
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)