import numpy as np

class LeastSquareSystem():
    is_solved = False
    zero_threshold = 1e-4

    def __init__(self, nvariables):
        self.nv = nvariables
        self.A_dct = []
        self.b = []

    def is_valid_var(self,ind):
        return (ind>=0) and (ind<self.nv)
    
    def add_constraint(self, coefs, result, weight=1):
        def is_valid_coef(i,v):
            return self.is_valid_var(i) \
                and (abs(v) > self.zero_threshold)

        dct = { i:weight*v for i,v in coefs.items() if is_valid_coef(i,weight*v) }
        if not dct:
            return False
        
        self.A_dct.append(dct)
        self.b.append(weight*result)
        
        self.is_solved = False

        return True

    def setup(self):        
        nvar = self.nv
        ncstr = len(self.A_dct)
        if (ncstr == 0) or (ncstr != len(self.b)):
            return None

        a_lss = np.zeros((ncstr, nvar))
        for idc,cstr in enumerate(self.A_dct):
            for idv,coef in cstr.items():
                a_lss[idc,idv] = coef

        return a_lss, np.asarray(self.b)

    def solve(self):
        lss = self.setup()
        if lss is None:
            return False
        
        x,res,rank,s = np.linalg.lstsq(*lss, rcond=None)
        self.solution = x
        self.residuals = res
        
        self.is_solved = True
        return True

class Curve2DLSS(LeastSquareSystem):
    def __init__(self, npoints):
        self.npts = npoints
        super().__init__(2*npoints)

    def add_constraint_pt(self, coefs, result, weight=1):
        coef_x = { (2*i):v for i,v in coefs.items() }
        coef_y = { (2*i+1):v for i,v in coefs.items() }
        constr_x = self.add_constraint(coef_x, result[0], weight)
        constr_y = self.add_constraint(coef_y, result[1], weight)
        return constr_x and constr_y

    def curve_solution(self):
        if (not self.is_solved) \
            and (not self.solve()):
            return None

        x = self.solution
        return [ [ x[2*i], x[2*i+1] ] for i in range(self.npts) ]
    
class Curve3DLSS(LeastSquareSystem):
    def __init__(self, npoints):
        self.npts = npoints
        super().__init__(3*npoints)

    def add_constraint_pt(self, coefs, result, weight=1):
        coef_x = { (3*i):v for i,v in coefs.items() }
        coef_y = { (3*i+1):v for i,v in coefs.items() }
        coef_z = { (3*i+2):v for i,v in coefs.items() }
        constr_x = self.add_constraint(coef_x, result[0], weight)
        constr_y = self.add_constraint(coef_y, result[1], weight)
        constr_z = self.add_constraint(coef_z, result[2], weight)
        return constr_x and constr_y and constr_z

    def curve_solution(self):
        if (not self.is_solved) \
            and (not self.solve()):
            return None

        x = self.solution
        return [ [ x[3*i], x[3*i+1], x[3*i+2] ] \
                    for i in range(self.npts) ]