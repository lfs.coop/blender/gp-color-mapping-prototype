import numpy as np
from .. common.color import rgb2lab

def optimize_linear(gpcm, weight_contrast, weight_mean, weight_anchor):
    mappings = gpcm.mappings
    active_mapping = gpcm.active_mapping()
    
    ''' Compute mappings in variables order : 
        the nb_driven first values are the unknown of our system.'''
    sorted_mappings  = [ m for m in mappings if m.is_driven(active_mapping)]
    nb_driven = len(sorted_mappings)
    sorted_mappings += [ m for m in mappings if not m.is_driven(active_mapping)]

    def is_driven(k):
        nonlocal nb_driven
        return k < nb_driven

    ''' Compute LAB values for mapped and reference colors.
        Note : We only need the mapped value of the mappings that are NOT variables.
        That is because the linear resolution does not need an initial solution. ''' 
    mapped_lab = nb_driven*[None] \
                    + [np.array(rgb2lab(m.get_mapped_default_color()[:3])) \
                            for m in sorted_mappings[nb_driven:]]
    ref_lab = [np.array(rgb2lab(m.get_source_default_color()[:3])) \
                    for m in sorted_mappings]
    
    groups = {}
    for k,m in enumerate(sorted_mappings):
        if not m.group in groups:
            groups[m.group] = []
        groups[m.group].append(k)

    def is_group_driven(group):
        return any([is_driven(k) for k in group])

    from . constraint_lss import Curve3DLSS
    ''' We use the Curve3D LSS because our variables have 
        3D values : the LAB coordinates of the unlocked mapped colors. '''
    lss = Curve3DLSS(nb_driven)

    ''' Contrast constraints '''
    def contrasts_coefs(k0, k1):
        coefs = {}
        b = ref_lab[k0] - ref_lab[k1]
        if is_driven(k0):
            coefs.update({k0:1})
        else:
            b += mapped_lab[k0]
        if is_driven(k1):
            coefs.update({k1:-1})
        else:
            b += mapped_lab[k1]
        return coefs, b        
    
    def contrast_weight(group):
        '''Normalized weight of a contrast constraint based on the number
        of contrast constraints applied on a color of a given group'''
        k = len([c for c in group if is_driven(c)])
        n = len(group)
        if n == 1 or k == 0: # n=1 -> k=0 or k=1
            return 0
        return 2/(k * (2*n - k - 1))
    
    weight_per_group = { group_ind:contrast_weight(group_maps) for group_ind, group_maps in groups.items() }
    contrast_constraints = [ (*contrasts_coefs(k0,k1),weight_per_group[group_ind]*weight_contrast) \
                                for group_ind, group_mappings in groups.items() \
                                for k0 in group_mappings \
                                for k1 in group_mappings if k0 < k1 ]
    contrast_constraints = list(filter(lambda x: len(x[0]) > 0, contrast_constraints))    
    for coefs, b, weight_contrast_norm in contrast_constraints:
        lss.add_constraint_pt(coefs, b, weight_contrast_norm)

    ''' Anchor constraints '''
    # weight_anchor = 0.1
    weight_anchor_norm = 1/nb_driven if nb_driven else 0
    for k,m in enumerate(sorted_mappings[:nb_driven]):
        lss.add_constraint_pt({k:1}, ref_lab[k], weight_anchor*weight_anchor_norm)
    
    ''' Mean constraint '''
    def mean_constraint(mappings_0, mappings_1):
        r0, r1 = 1/len(mappings_0), -1/len(mappings_1)
        coefs = { k:r0 for k in mappings_0 if is_driven(k) }
        coefs.update({ k:r1 for k in mappings_1 if is_driven(k) })
        b  = sum([ ref_lab[k]*r0 for k in mappings_0 ])
        b += sum([ ref_lab[k]*r1 for k in mappings_1 ])
        b += -sum([ mapped_lab[k]*r0 for k in mappings_0 if not is_driven(k) ])
        b += -sum([ mapped_lab[k]*r1 for k in mappings_1 if not is_driven(k) ])
        return coefs, b
    
    mean_group_constraints = [ mean_constraint(g0_mappings, g1_mappings)\
                            for g0, g0_mappings in groups.items() \
                            for g1, g1_mappings in groups.items() \
                                if g0 < g1 and is_group_driven(g0_mappings) and is_group_driven(g1_mappings) ]
    mean_group_constraints = list(filter(lambda x: len(x[0]) > 0, mean_group_constraints))
    weight_mean_norm = 1/len(mean_group_constraints) if mean_group_constraints else 0
    for coefs, b in mean_group_constraints:
        lss.add_constraint_pt(coefs, b, weight_mean*weight_mean_norm)

    if not lss.solve():
        ''' System not solvable : leave the colors as initial values '''
        return [cc for m in sorted_mappings[:nb_driven] \
                    for cc in rgb2lab(m.get_mapped_default_color()[:3])]


    return lss.solution