import bpy
from bpy.props import *
from .. common.color import rgb2lab, rgba2hex
from .. picking.picking_maths import pick_material_under

class GPCOLORMAP_OT_explorerPickMaterial(bpy.types.Operator):
    bl_idname = "scene.explorer_pick_material"
    bl_label = "Pick Material"
    bl_description = "Pick material from viewport"

    bl_options = {'REGISTER', 'DEPENDS_ON_CURSOR', 'UNDO'}
    bl_cursor_pending = 'EYEDROPPER'

    def execute(self, context):
        return {'FINISHED'}

    def invoke(self, context, event):
        cursor = event.mouse_region_x, event.mouse_region_y
        picked_mat = pick_material_under(context, cursor)
        if picked_mat is None:
            return {'CANCELLED'}

        gpcm = context.scene.gpcolormapping
        map_ind = gpcm.index(picked_mat)
        if map_ind < 0:
            # Mapping do not exist => create it !
            gpcm.pending_material = picked_mat
            if not gpcm.accept_pending(context, set_active=True):
                self.report({'WARNING'}, "Mapping was not accepted")
                return {'CANCELLED'}
        else:
            # Mapping exists => set as active
            gpcm.active_index = map_ind
        # bpy.ops.scene.explore_stroke_colors('INVOKE_DEFAULT')
       
        if gpcm.autogroup:
            gpcm.mappings[map_ind].group = 1
       
        return {'FINISHED'}

class GPCOLORMAP_OT_explorerClearColors(bpy.types.Operator):
    bl_idname = "scene.explorer_clear_colors"
    bl_label = "Clear Colors"
    bl_description = "Remove all colors"

    def execute(self, context):
        gpcm = context.scene.gpcolormapping
        gpcm.active_index = -1
        for mpg in gpcm.mappings:
            mpg.is_locked = True
        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_confirm(self, event)

class GPCOLORMAP_OT_explorerAddActiveGroupColors(bpy.types.Operator):
    bl_idname = "scene.explorer_add_active_group_colors"
    bl_label = "Add Active Group Colors"
    bl_description = "Add all colors in the active color group to the list"

    @classmethod
    def poll(cls, context):
        gpcm = context.scene.gpcolormapping
        active = gpcm.active_mapping()
        remaining_locked = []
        if active is not None:
            remaining_locked = [m for m in gpcm.mappings \
                                if m != active and m.group == active.group and m.is_locked]
        return len(remaining_locked) > 0

    def execute(self, context):
        gpcm = context.scene.gpcolormapping
        active = gpcm.active_mapping()
        if active is None:
            return {'FINISHED'}
        
        for mpg in gpcm.mappings:
            if active == mpg or mpg.group != active.group:
                continue
            mpg.is_locked = False
        return {'FINISHED'}

class GPCOLORMAP_OT_explorerCreateGroup(bpy.types.Operator):
    bl_idname = "scene.explorer_create_group"
    bl_label = "New Color Group"
    bl_description = "Group all colors in a new group"

    @classmethod
    def poll(cls, context):
        return not context.scene.gpcolormapping.is_empty()

    def execute(self, context):
        gpcm = context.scene.gpcolormapping
        groups = set([m.group for m in gpcm.mappings])
        new_group = max(groups, default=-1) + 1
        for mpg in gpcm.mappings:
            if mpg.is_locked:
                continue
            mpg.group = new_group
        return {'FINISHED'}

class GPCOLORMAP_OT_explorePaletteColors(bpy.types.Operator):
    bl_idname = "scene.explore_palette_colors"
    bl_label = "Explore Palette Colors"
    bl_description = "Pick Grease Pencil materials and edit their colors using the Grease Pencil Color Explorer"
    bl_options = {'REGISTER', 'DEPENDS_ON_CURSOR', 'UNDO'}
    bl_cursor_pending = 'EYEDROPPER'


    def execute(self, context):
        # Restore original locks
        gpcm = context.scene.gpcolormapping
        gpcm.lock_inactive_groups = self.orig_lock_grp
        for i, is_locked in enumerate(self.orig_lock):
            gpcm.mappings[i].is_locked = (is_locked and i != gpcm.active_index)
        return {'FINISHED'}

    def invoke(self, context, event):
        cursor = event.mouse_region_x, event.mouse_region_y
        picked_mat = pick_material_under(context, cursor)
        if picked_mat is None:
            self.report({'WARNING'}, "Mapping was not accepted")
            return {'CANCELLED'}

        gpcm = context.scene.gpcolormapping
        # Save mapping locks to restore them after execution
        self.orig_lock = [m.is_locked for m in gpcm.mappings]
        self.orig_lock_grp = gpcm.lock_inactive_groups
        map_ind = gpcm.index(picked_mat)
        if map_ind < 0:
            # Mapping does not exist => create it !
            gpcm.pending_material = picked_mat
            if not gpcm.accept_pending(context, set_active=True):
                self.report({'WARNING'}, "Mapping was not accepted")
                return {'CANCELLED'}
        else:
            # Mapping exists => set as active
            gpcm.active_index = map_ind
        # Deactivate inactive groups auto-lock
        gpcm.lock_inactive_groups = False
        # Lock all but picked material
        for mpg in gpcm.mappings:
            if mpg == gpcm.active_mapping():
                if gpcm.autogroup:
                    mpg.group = 1
                continue
            mpg.is_locked = True
            if gpcm.autogroup:
                mpg.group = 0
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=350)

    def draw(self, context):
        layout = self.layout

        cmap = context.scene.gpcolormapping
        active_mapping = cmap.active_mapping()

        '''-- Splitting the layout in 2 columns --'''
        cont = layout.split(factor=0.4)
        
        '''== 1st column - picker =='''
        col = cont.column()
        
        if active_mapping is None:
            row = col.row()
            row.label(text="No active mapping")
        else:
            base_mat = active_mapping.base_material

            ''' 1st row '''
            row = col.row()
            row.label(text=f'{base_mat.name}')

            ''' 2nd row '''
            row = col.row()
            row.template_color_picker(cmap, "driving_color", value_slider=True)

            ''' 3rd row '''
            row = col.row()
            row.prop(cmap, "driving_color", icon_only=True)

        '''== 2nd column - mapping list =='''
        col = cont.column()
        
        row = col.row()
        row.ui_units_x = 2
        row.template_list("GPCOLORMAP_UL_ColorExplorerDialogList", 'GP_ColorMapping', \
                        dataptr=cmap, propname="mappings", \
                        active_dataptr=cmap, active_propname="active_index", type='GRID', columns=1)

        row = col.row(align=True)

        col = row.column()
        col.scale_x = 1.1
        col.operator("scene.explorer_create_group", text='', icon='GROUP', emboss=False)

        col = row.column()
        col.scale_x = 1.1
        col.operator("scene.explorer_add_active_group_colors", text='', icon='ADD', emboss=False)
        
        col = row.column()
        col.scale_x = 3
        col.operator("scene.explorer_pick_material", text='', icon='EYEDROPPER')

        col = row.column()
        col.scale_x = 1.1
        col.operator("scene.explorer_clear_colors", text='', icon='TRASH', emboss=False)

def menu_func(self, context):
    self.layout.operator_context = 'INVOKE_DEFAULT'
    self.layout.operator("scene.explore_stroke_colors")

classes = [GPCOLORMAP_OT_explorerClearColors, \
           GPCOLORMAP_OT_explorerAddActiveGroupColors, \
           GPCOLORMAP_OT_explorerCreateGroup, \
           GPCOLORMAP_OT_explorerPickMaterial, \
           GPCOLORMAP_OT_explorePaletteColors]

def register():
    for cls in classes:
        bpy.utils.register_class(cls) 
    
    bpy.types.VIEW3D_MT_gpencil_edit_context_menu.append(menu_func)

def unregister():        
    bpy.types.VIEW3D_MT_gpencil_edit_context_menu.remove(menu_func)

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)