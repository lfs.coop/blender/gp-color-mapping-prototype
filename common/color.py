import sys

def hex2rgba(hex):
    h = hex.lstrip('#')
    irgba = [int(h[2*i:2*i+2], 16) for i in range(len(h)//2)]
    return [ float(x)/255. for x in irgba ]

def rgba2hex(rgba):
    hex = '#'
    for k in range(len(rgba)):
        hex += '{:02x}'.format( int(255*rgba[k]) )
    return hex

def rgb2gray(rgb):
    return 0.2126*rgb[0] + 0.7152*rgb[1] + 0.0722*rgb[2]

def rgb2hsv(rgb):
    cmin = min(rgb)
    cmax = max(rgb)
    v = cmax
    s = 0
    h = 0

    if cmax > 0:
        s = 1.0 - cmin/cmax
    if cmin == cmax:
        t = 0
    elif cmax == rgb[0]:
        h = (60.0 * (rgb[1]-rgb[2])/(cmax-cmin) + 360.0) % 360.0
    elif cmax == rgb[1]:
        h = 60.0 * (rgb[2]-rgb[0])/(cmax-cmin) + 120.0
    elif cmax == rgb[2]:
        h = 60.0 * (rgb[0]-rgb[1])/(cmax-cmin) + 240.0
    return [h,s,v]

def rgb2hsv_h(rgb):
    cmin = min(rgb)
    cmax = max(rgb)
    h = 0
    if cmax == rgb[0]:
        h = (60.0 * (rgb[1]-rgb[2])/(cmax-cmin) + 360.0) % 360.0
    elif cmax == rgb[1]:
        h = 60.0 * (rgb[2]-rgb[0])/(cmax-cmin) + 120.0
    elif cmax == rgb[2]:
        h = 60.0 * (rgb[0]-rgb[1])/(cmax-cmin) + 240.0
    return h

def rgb2xyz_x(rgb):
    return 0.4124564 * rgb[0] + 0.3575761 * rgb[1] + 0.1804375 * rgb[2]

def rgb2xyz_y(rgb):
    return 0.2126729 * rgb[0] + 0.7151522 * rgb[1] + 0.0721750 * rgb[2]

def rgb2xyz_z(rgb):
    return 0.0193339 * rgb[0] + 0.1191920 * rgb[1] + 0.9503041 * rgb[2]

def rgb2xyz(rgb):
    # sRGB to XYZ
    return (
        100.0 * rgb2xyz_x(rgb),
        100.0 * rgb2xyz_y(rgb),
        100.0 * rgb2xyz_z(rgb),
    )

def xyz2rgb(xyz):
    # XYZ to sRGB
    r =  3.2404542 * xyz[0] - 1.5371385 * xyz[1] - 0.4985314 * xyz[2]
    g = -0.9692660 * xyz[0] + 1.8760108 * xyz[1] + 0.0415560 * xyz[2]
    b =  0.0556434 * xyz[0] - 0.2040259 * xyz[1] + 1.0572252 * xyz[2]
    return [0.01 * i for i in [r,g,b]]

def f_lab(t):
    if t > pow(6.0/29.0, 3):
        return pow(t, 1.0/3.0)
    else:
        return (1.0/3.0) * pow(29.0/6.0, 2) * t + (4.0/29.0)

def f_lab_inv(t):
    if t > 6.0/29.0:
        return pow(t, 3)
    else:
        return 3.0 * pow(6.0/29.0, 2) * (t - (4.0/29.0))

def rgb2lab_l(rgb, yn=100.0):
    y = 100.0 * rgb2xyz_y(rgb)
    l = 116.0 * f_lab(y/yn) - 16.0
    return l

def rgb2lab_a(rgb, xn=95.0489, yn=100.0):
    x = 100.0 * rgb2xyz_x(rgb)
    y = 100.0 * rgb2xyz_y(rgb)
    a = 500.0 * (f_lab(x/xn) - f_lab(y/yn))
    return a

def rgb2lab_b(rgb, yn=100.0, zn=108.884):
    y = 100.0 * rgb2xyz_y(rgb)
    z = 100.0 * rgb2xyz_z(rgb)
    b = 200.0 * (f_lab(y/yn) - f_lab(z/zn))
    return b

def lab_min_max():
    min_a = sys.maxsize
    min_b = sys.maxsize
    max_l = 0.0
    max_a = 0.0
    max_b = 0.0
    for r in (0, 1):
        for g in (0, 1):
            for b in (0, 1):
                lab_l = rgb2lab_l((r, g, b), 100.0)
                lab_a = rgb2lab_a((r, g, b), 95.0489, 100.0)
                lab_b = rgb2lab_b((r, g, b), 100.0, 108.884)
                min_a = min(min_a, lab_a)
                min_b = min(min_b, lab_b)
                max_l = max(max_l, lab_l)
                max_a = max(max_a, lab_a)
                max_b = max(max_b, lab_b)
    return (0.0, min_a, min_b), (max_l, max_a, max_b)

# LAB_MIN, LAB_MAX = lab_min_max()
# LAB_MAX_L = LAB_MAX[0]
# LAB_RANGE_A = LAB_MAX[1] - LAB_MIN[1]
# LAB_RANGE_B = LAB_MAX[2] - LAB_MIN[2]

def lab2xyz(lab, xn=95.0489, yn=100.0, zn=108.884):
    # Reference values for standard achromatic illuminant D65
    # From https://en.wikipedia.org/wiki/CIELAB_color_space#From_CIELAB_to_CIEXYZ
    x = xn * f_lab_inv((lab[0] + 16.0)/116.0 + lab[1]/500.0)
    y = yn * f_lab_inv((lab[0] + 16.0)/116.0)
    z = zn * f_lab_inv((lab[0] + 16.0)/116.0 - lab[2]/200.0)
    return [x,y,z]

def xyz2lab(xyz, xn=95.0489, yn=100.0, zn=108.884):
    # Reference values for standard achromatic illuminant D65
    # From https://en.wikipedia.org/wiki/CIELAB_color_space#From_CIEXYZ_to_CIELAB
    l = 116.0 * f_lab(xyz[1]/yn) - 16.0
    a = 500.0 * (f_lab(xyz[0]/xn) - f_lab(xyz[1]/yn))
    b = 200.0 * (f_lab(xyz[1]/yn) - f_lab(xyz[2]/zn))
    return [l,a,b]

def rgb2lab(rgb):
    return xyz2lab(rgb2xyz(rgb))

def lab2rgb(lab):
    return xyz2rgb(lab2xyz(lab))

def is_same_color(color1, color2):
    return rgba2hex(color1) == rgba2hex(color2)