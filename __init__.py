# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import bpy

bl_info = {
    "name": "GP Color Mapping",
    "author": "Les Fées Spéciales (LFS)",
    "description": "",
    "blender": (3, 0, 0),
    "version": (0, 0, 0),
    "location": "",
    "category": "Material"
}


import bpy
from bpy.props import *

class COLORMAPPER_OT_PackageInstall(bpy.types.Operator):
    """Install single package"""
    bl_idname = "preferences.colormapper_package_install"
    bl_label = "Install package"

    package: StringProperty(name="Package name")

    def install_package(self): 
        import subprocess, sys, os

        python_path = sys.executable
        subp = subprocess.run([python_path, "-m", "ensurepip"])
        if subp.returncode != 0:
            return False
        subp = subprocess.run([python_path, "-m", "pip", "install", \
                            #    "--target=" + os.path.join(self.install_path,
                                                        #   "modules"),
                               self.package, "--upgrade"])
        if subp.returncode != 0:
            return False
        return True
    
    def execute(self, context):
        done = self.install_package()
        if not done :
            return {'CANCELLED'}
        
        prefs = context.preferences.addons[__package__].preferences
        prefs.refresh_deps()
        return  {'FINISHED'} 
    
''' --- Overall Addon preferences --- '''
class COLORMAPPER_preferences(bpy.types.AddonPreferences):
    bl_idname = __name__

    scipy_lib_found: BoolProperty(name="scipy lib is installed", default=False)
    weight_contrast: FloatProperty(name="Contrast Weight", description="Allow to control the global influence of contrast constraints", default=1, min=0)
    weight_mean: FloatProperty(name="Mean Contrast Weight", description="Allow to control the global influence of mean contrast constraints", default=1, min=0)
    weight_anchor: FloatProperty(name="Anchor Weight", description="Allow to control the global influence of anchor constraints", default=0.1, min=0)

    def refresh_deps(self):
        try:
            import scipy
            self.scipy_lib_found = True
        except ModuleNotFoundError:
            self.scipy_lib_found = False  

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.label(text="Scipy library")
        text = "Not found" if not self.scipy_lib_found else "Library is installed"
        icon = "ERROR" if not self.scipy_lib_found else 'CHECKMARK'
        row = layout.row()
        col = row.column()
        col.alert = (not self.scipy_lib_found)
        col.label(text=text, icon=icon)

        if not self.scipy_lib_found:
            col = row.column()
            op_install_pck = col.operator("preferences.colormapper_package_install")
            op_install_pck.package = "scipy"
        
        layout.separator()

        '''Constraint global weights'''
        row = layout.row()
        row.label(text="Constraint weights")

        row = layout.row()
        col = row.column()
        col.label(text="Contrasts")
        col = row.column()
        col.prop(self, "weight_contrast", text="")

        row = layout.row()
        col = row.column()
        col.label(text="Mean contrasts")
        col = row.column()
        col.prop(self, "weight_mean", text="")

        row = layout.row()
        col = row.column()
        col.label(text="Anchors")
        col = row.column()
        col.prop(self, "weight_anchor", text="")
    
''' --- Class Registration --- '''
classes = (COLORMAPPER_OT_PackageInstall, COLORMAPPER_preferences)
addon_keymaps = []

def register():
    for cls in classes:
        bpy.utils.register_class(cls) 
    
    from . common import register as register_common
    register_common()

    from . constraint import register as register_constraint
    register_constraint()

    from . mapping import register as register_mapping
    register_mapping()

    from . picking import register as register_picking
    register_picking()

    from . navigate import register as register_navigate
    register_navigate()

def unregister():       
    # Remove the hotkey
    for km, kmi, dsc in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()

    from . navigate import unregister as unregister_navigate
    unregister_navigate()

    from . picking import unregister as unregister_picking
    unregister_picking()

    from . mapping import unregister as unregister_mapping
    unregister_mapping() 

    from . constraint import unregister as unregister_constraint
    unregister_constraint()

    from . common import unregister as unregister_common
    unregister_common()

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
        
if __name__ == "__main__":
    register() 
