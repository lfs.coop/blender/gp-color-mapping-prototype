import bpy
from bpy.props import *
from .. common.color import rgb2gray

class GPCOLORMAP_OT_grayscalePreset(bpy.types.Operator):
    bl_idname = "scene.grayscale_color_mapping"
    bl_label = "Grayscale"
    bl_description = "Map GP materials to their grayscale equivalent"

    from_source: BoolProperty(name="From Source", default=True)

    @classmethod
    def poll(cls, context):
        return not context.scene.gpcolormapping.is_empty()

    def execute(self, context):   
        gpcm = context.scene.gpcolormapping

        for mapping in gpcm.mappings:

            if self.from_source:
                mat = mapping.base_material.grease_pencil
                input_fill = mat.fill_color
                input_stroke = mat.color
            else:
                input_fill = mapping.fill_color
                input_stroke = mapping.stroke_color

            mapping.fill_color = [rgb2gray(input_fill) for rgb in range(3)]
            mapping.stroke_color = [rgb2gray(input_stroke) for rgb in range(3)]

        return {'FINISHED'}

classes = [ GPCOLORMAP_OT_grayscalePreset ]

def register():
    for cls in classes:
        bpy.utils.register_class(cls) 

def unregister():        
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)