    
''' --- Class Registration --- '''
def register():    
    from . colormap_props import register as register_props
    register_props()

    from . colormap_ops import register as register_ops
    register_ops()

    from . colormap_presets import register as register_pst
    register_pst()

    from . colormap_panel import register as register_panel
    register_panel()

def unregister():         
    from . colormap_panel import unregister as unregister_panel
    unregister_panel()  
    
    from . colormap_presets import unregister as unregister_pst
    unregister_pst()

    from . colormap_ops import unregister as unregister_ops
    unregister_ops()

    from . colormap_props import unregister as unregister_props
    unregister_props()

if __name__ == "__main__":
    register() 
