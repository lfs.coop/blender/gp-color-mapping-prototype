import bpy
from collections import defaultdict
from bpy.types import PropertyGroup, Material
from bpy.props import *
from .. common.color import rgba2hex, is_same_color, lab2rgb
from .. navigate.navigate_maths import *
from .. constraint.constraint_maths import optimize_linear

mapping_color_mode = [('FILL','FILL','Fill color only'), \
             ('STROKE','STROKE','Stroke color only'), \
             ('BOTH','BOTH', 'Fill and Stroke colors'), \
             ('NONE','NONE', 'Empty')]

class GPColorMappingModifier(PropertyGroup):
    bl_idname="scene.gpcolormapping_modifier"

    def update_active(self, context):
        for ob,mdf in self.get_modifiers(context):
            mdf.show_render = self.is_active
            mdf.show_in_editmode = self.is_active
            mdf.show_viewport = self.is_active

    def update_target_color(self, context):   
        for ob,mdf in self.get_modifiers(context): 
            mdf.color = self.target_color[0:3]

    name: StringProperty(name="Name")
    is_active: BoolProperty(name="Active", default=True, update=update_active)
    target_color: FloatVectorProperty(subtype='COLOR', name="Target Color", min=0, max=1, size=3, default=(0.,0.,0.), update=update_target_color)
    mdf_mode: EnumProperty(items=mapping_color_mode, name="Material Color Mode")

    def link_material(self, context, material):
        for ob,mdf in self.get_modifiers(context):
            if material.name in ob.material_slots:
                mdf.material = material
            else:
                ob.grease_pencil_modifiers.remove(mdf)

    def create_modifier(self, ob, material):
        if (material is None) \
            or (not material.name in ob.material_slots):
            return None

        mdfs = ob.grease_pencil_modifiers

        if self.name in mdfs:
            mdfs.remove(mdfs[self.name])    

        mdf = mdfs.new(name=self.name, type='GP_TINT')
        mdf.factor = 1
        mdf.material = material
        mdf.vertex_mode = self.mdf_mode
        self.name = mdf.name  

        return mdf

    def init(self, context, material, mode='BOTH'):    
        self.name = material.name + "_" + mode 
        self.mdf_mode = mode

        for ob in context.scene.objects:
            if (not ob) or (ob.type != 'GPENCIL'):
                continue       
            self.create_modifier(ob, material)

        if mode == 'FILL':
            self.target_color = [c for c in material.grease_pencil.fill_color[0:3]]
        else:
            self.target_color = [c for c in material.grease_pencil.color[0:3]]
        return self.target_color
    
    def refresh(self, context, material):
        uptodate = True
        for ob in context.scene.objects:
            if (not ob) or (ob.type != 'GPENCIL'):
                continue
            
            mdfs = ob.grease_pencil_modifiers
            if not self.name in mdfs:
                mdf = self.create_modifier(ob,material)
                if mdf is None:
                    continue
                mdf.color = self.target_color[0:3]
                uptodate = False
        return uptodate

    def get_modifiers(self, context):   
        mdfs = []
        for ob in context.scene.objects:
            if (not ob) or (ob.type != 'GPENCIL'):
                continue

            if self.name in ob.grease_pencil_modifiers:
                mdfs.append((ob,ob.grease_pencil_modifiers[self.name]))

        return mdfs
    
    def clear(self,context):
        for ob in context.scene.objects:
            if (not ob) or (ob.type != 'GPENCIL'):
                continue
                
            mdfs = ob.grease_pencil_modifiers
            if self.name in mdfs:
                ''' Trick to avoid decreasing the material's users 
                    while removing the modifier. 
                    Indeed, when the modifier is added, the material's 
                    user count is not increased (due to a bug I guess). '''
                mdfs[self.name].material = None
                mdfs.remove(mdfs[self.name])

class GPColorMappingItem(PropertyGroup):
    bl_idname="scene.gpcolormapping_item"   

    def update_base_material(self, context): 
        self.name = self.base_material.name

        if not self.is_initialized:
            self.reset(context)
            self.is_initialized = True
        else:
            self.fill_modifier.link_material(context, self.base_material)
            self.stroke_modifier.link_material(context, self.base_material)

    def update_active(self, context):
        self.fill_modifier.is_active = self.is_active
        self.stroke_modifier.is_active = self.is_active

    def update_linked(self, context):
        if self.is_linked:
            self.fill_color = self.stroke_color

    def is_driven(self, active_mapping):
        return not self.is_locked and self != active_mapping

    name: StringProperty(name="Name")

    base_material: PointerProperty(type=Material, name="Base Material", update=update_base_material)

    is_initialized: BoolProperty(default=False)
    is_active: BoolProperty(name="Active", default=True, update=update_active)
    is_linked: BoolProperty(name="Linked", default=False, update=update_linked)

    is_locked: BoolProperty(name="Locked", default=False)

    fill_modifier: PointerProperty(type=GPColorMappingModifier, name="Modifier")
    stroke_modifier: PointerProperty(type=GPColorMappingModifier, name="Modifier")

    group: IntProperty(name="Group Index", default=0, min=0)

    def set_fill_color(self, value):
        self.fill_modifier.target_color = value
        if (not self.is_linked) or is_same_color(value, self.stroke_color):
            return
        self.stroke_color = value

    def get_fill_color(self):
        return self.fill_modifier.target_color
        
    fill_color: FloatVectorProperty(subtype='COLOR', name="Target Fill Color", min=0, max=1, size=3, set=set_fill_color, get=get_fill_color)


    def set_stroke_color(self, value):
        self.stroke_modifier.target_color = value
        if (not self.is_linked) or is_same_color(value, self.fill_color):
            return
        self.fill_color = value

    def get_stroke_color(self):
        return self.stroke_modifier.target_color
    
    stroke_color: FloatVectorProperty(subtype='COLOR', name="Target Stroke Color", min=0, max=1, size=3, set=set_stroke_color, get=get_stroke_color)

    def has_fill(self):
        return self.gpmaterial().show_fill
    
    def has_stroke(self):
        return self.gpmaterial().show_stroke

    def has_fill(self):
        return self.gpmaterial().show_fill
    
    def has_stroke(self):
        return self.gpmaterial().show_stroke

    def copy(self, other, copy_name=True):
        if copy_name:
            self.name = other.name
        self.base_material = other.base_material
        self.is_linked = other.is_linked
        self.fill_color = other.fill_color
        self.stroke_color = other.stroke_color

    def gpmaterial(self):
        return self.base_material.grease_pencil

    def is_identity(self):
        gpm = self.gpmaterial()

        if gpm.show_stroke:
            if not is_same_color(gpm.color[0:3], self.stroke_color):
                return False
        
        if gpm.show_fill:
            if not is_same_color(gpm.fill_color[0:3], self.fill_color):
                return False
                
        return True

    def clear(self, context):
        self.fill_modifier.clear(context)
        self.stroke_modifier.clear(context)

    def check_material(self, context):
        self.is_initialized = True

        if self.base_material is None:
            mats = bpy.data.materials
            if (not self.name in mats) \
                or (mats[self.name] is None) \
                or (not mats[self.name].is_grease_pencil):
                return False
            self.base_material = mats[self.name] 
        return True

    def refresh(self, context):
        self.fill_modifier.refresh(context, self.base_material)
        self.stroke_modifier.refresh(context, self.base_material)

    def reset(self, context):    
        if not self.check_material(context):
            return 

        self.is_linked = (rgba2hex(self.gpmaterial().color) \
            == rgba2hex(self.gpmaterial().fill_color))

        self.fill_color = self.fill_modifier.init(context, self.base_material, mode="FILL")
        self.stroke_color = self.stroke_modifier.init(context, self.base_material, mode="STROKE")
    

    def apply(self,context):
        if not self.is_active:
            return
        mat = self.gpmaterial()
        if mat.show_stroke:
            mat.color[0:3] = self.stroke_color
        if mat.show_fill:
            mat.fill_color[0:3] = self.fill_color    

    def get_source_default_color(self):
        if self.has_fill():
            return self.gpmaterial().fill_color
        elif self.has_stroke():
            return self.gpmaterial().color
        else:
            return None

    def get_mapped_default_color(self):
        if self.has_fill():
            return self.fill_color
        elif self.has_stroke():
            return self.stroke_color
        else:
            return None

    def set_mapped_default_color(self, value):
        if self.has_fill():
            self.fill_color = value
        elif self.has_stroke():
            self.stroke_color = value

class GPColorMapping(PropertyGroup):
    bl_idname= "scene.gpcolormapping"

    def driving_use_fill(self):
        return (self.driving_color_mode != {"LINE"})

    def update_active(self, context):
        for mapping in self.mappings:
            mapping.is_active = self.is_active
    
    def update_active_index(self, context):
        mapping = self.active_mapping()
        if mapping is None:
            return
        mapping.is_locked = False
        if self.driving_mode == 'CP' and self.lock_inactive_groups:
            for m in self.mappings:
                m.is_locked = (m.group != mapping.group)

    is_active: BoolProperty(name="Active", default=True, update=update_active)
    mappings: CollectionProperty(name="Mappings", type=GPColorMappingItem, override={'USE_INSERTION'})
    active_index: IntProperty(name="Active palette index", default=-1, update=update_active_index)

    pending_material: PointerProperty(type=bpy.types.Material)

    def set_driving_color(self, value):
        mapping = self.active_mapping()
        if mapping is None:
            return
        
        use_fill = mapping.has_fill()         
        map_color = mapping.fill_color if use_fill\
               else mapping.stroke_color
        old_color = map_color.copy()
    
        if rgba2hex(map_color) == rgba2hex(value):
            return

        if use_fill:
            mapping.fill_color = value
        else:
            mapping.stroke_color = value
        
        if self.driving_mode == 'HSV':
            driven_colors = self.get_mapped_colors(True)
            drive_hsv(old_color, map_color, driven_colors)
        elif self.driving_mode == 'CP':
            prefs = bpy.context.preferences.addons[__package__.split('.', 1)[0]].preferences
            wc, wm, wa = prefs.weight_contrast, prefs.weight_mean, prefs.weight_anchor
            self.set_mapped_colors(optimize_linear(self, wc, wm, wa))

    def get_driving_color(self):
        mapping = self.active_mapping()
        if mapping is None:
            return 3*[0.]       
        
        return mapping.fill_color if mapping.has_fill()\
               else mapping.stroke_color

    def get_enum_driving_color_mode(self, context):    
        line_item = ('LINE', 'Line', 'Line','SHADING_RENDERED')
        fill_item = ('FILL', 'Fill', 'Fill','SHADING_SOLID')
        linked_item = ('LINKED', 'Linked', 'Fill & Line','SHADING_TEXTURE',1)

        mapping = self.active_mapping()
        if mapping is None:
            return [linked_item]
        
        if mapping.is_linked:
            return [linked_item]
        
        if not mapping.has_fill():
            return [line_item + (1,)]
        
        if not mapping.has_stroke():
            return [fill_item + (1,)]

        return [line_item + (1,), fill_item + (2,)]

    def update_lock_inactive_groups(self, context):
        if not self.lock_inactive_groups:
            return

        mapping = self.active_mapping()
        for m in self.mappings:
            m.is_locked = (m.group != mapping.group)

    driving_color: FloatVectorProperty(subtype='COLOR', name="Driving Color", min=0, max=1, size=3, default=(0.,0.,0.), get=get_driving_color, set=set_driving_color)

    def set_driving_color_mode(self, value):
        ''' Read-only property '''
        return
    
    def get_driving_color_mode(self):
        mapping = self.active_mapping()
        if mapping is None:
            return 4
        
        if mapping.has_fill() and mapping.has_stroke():
            return 3
        
        if (not mapping.has_fill()) and (not mapping.has_stroke()):
            return 4
        
        return 2 if mapping.has_fill() else 1
    
    enum_driving_color_mode = [('LINE', 'Line', 'Line','SHADING_RENDERED',1), \
                               ('FILL', 'Fill', 'Fill','SHADING_SOLID',2), \
                               ('LINKED', 'Linked', 'Fill & Line','SHADING_TEXTURE',3), \
                               ('NONE', 'None', 'None', 'SHADING_BBOX', 4)]
    driving_color_mode: EnumProperty(items=enum_driving_color_mode, get=get_driving_color_mode, set=set_driving_color_mode)

    enum_driving_mode = [('HSV', 'HSV', "Apply the same HSV translation of the driving color."), \
                         ('CP', 'Constrained Palette', "Update colors according to their constraints.")]
    driving_mode: EnumProperty(name='Driving mode', items=enum_driving_mode, default='CP')
    lock_inactive_groups: BoolProperty(name='Autolock inactive groups', default=False, update=update_lock_inactive_groups, \
        description="Automatically lock all colors outside the group of the currently edited color")
    
    autogroup: BoolProperty(name="Autogroup", description="Automatically deal with group with exploring mappings", default=True)

    def active_mapping(self):
        if (self.active_index < 0) or (self.active_index >= len(self.mappings)):
            return None
        return self.mappings[self.active_index]

    def is_empty(self):
        return len(self.mappings) == 0

    def index(self, material):
        try:
            return ([m.base_material for m in self.mappings]).index(material)
        except ValueError:
            return -1

    def contains_material(self, material):
        return material in [m.base_material for m in self.mappings]

    def is_material_available(self, material):
        return (not material is None) and (material.is_grease_pencil) \
            and (not self.contains_material(material))

    def accept_pending(self, context, set_active=False):    
        if not self.is_material_available(self.pending_material):
            self.pending_material = None
            return False
        mapping = self.mappings.add()
        mapping.base_material = self.pending_material
        if set_active:
            self.active_index= len(self.mappings)-1
        self.pending_material = None
        return True

    def remove_mapping(self, context, mapping_id):
        if (mapping_id < 0) or \
            (mapping_id >= len(self.mappings)):
            return
        self.mappings[mapping_id].clear(context)
        self.mappings.remove(mapping_id)
        if (mapping_id == self.active_index) or \
            (self.active_index >= len(self.mappings)):
            self.active_index = -1

    def remove_all(self, context):
        n = len(self.mappings)
        for mapping_id in reversed(range(n)):
            self.remove_mapping(context, mapping_id)

    def apply_mapping(self, context, mapping_id, remove=True):
        if (mapping_id < 0) or \
            (mapping_id >= len(self.mappings)):
            return
        self.mappings[mapping_id].apply(context)
        if remove:
            self.remove_mapping(context, mapping_id)

    def apply_all(self, context):
        n = len(self.mappings)
        for mapping_id in range(n):
            self.apply_mapping(context, mapping_id, remove=False)

        self.remove_all(context)

    def refresh(self, context):
        ind = 0
        while ind < len(self.mappings):
            if not self.mappings[ind].check_material(context):
                self.remove_mapping(context, ind)
            else:
                self.mappings[ind].refresh(context)
                ind += 1

    def reset(self, context):
        ind = 0
        while ind < len(self.mappings):
            if not self.mappings[ind].check_material(context):
                self.remove_mapping(context, ind)
            else:
                self.mappings[ind].reset(context)
                ind += 1

    def get_mapped_colors(self, driven_only=False):
        '''
        Return the mapping's current target colors as a
        flatten list of RGB color coordinates.
        
        TODO: handle stroke and fill separately
        '''
        mappings = [m for m in self.mappings]
        if driven_only:
            mappings = [m for m in mappings \
                if m.is_driven(self.active_mapping())]
        colors = [m.get_mapped_default_color() \
            for m in mappings]
        return colors

    def set_mapped_colors(self, colors):
        '''
        Set the mapping's current target colors with the
        given list of RGB color coordinates.
        
        TODO: handle stroke and locked colors
        '''
        i = 0
        for m in self.mappings:
            if m.is_driven(self.active_mapping()):
                m.set_mapped_default_color(lab2rgb(colors[i:i+3]))
                i += 3


classes = [GPColorMappingModifier, GPColorMappingItem, GPColorMapping]

def register():
    for cls in classes:
        bpy.utils.register_class(cls) 
    bpy.types.Scene.gpcolormapping = PointerProperty(type=GPColorMapping)

def unregister():        
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)