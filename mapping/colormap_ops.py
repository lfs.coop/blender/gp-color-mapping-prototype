import bpy, json
from bpy.props import *
from bpy_extras.io_utils import ExportHelper, ImportHelper

class GPCOLORMAP_OT_applyMapping(bpy.types.Operator):
    bl_idname = "scene.apply_mapping"
    bl_label = "Apply mapping"
    bl_description = "Apply mapping"

    mapping_id: IntProperty(name="Mapping ID", default=-1)

    def execute(self, context):   
        gpcm = context.scene.gpcolormapping

        if self.mapping_id >= 0:
            gpcm.apply_mapping(context,self.mapping_id)
        else:
            gpcm.apply_all(context)     

        bpy.ops.ed.undo_push()
        return {'FINISHED'}   
      
mode_add_enum = [('PENDING', 'PENDING', 'Pending material only'), \
                 ('ACTIVE', 'ACTIVE', 'All materials in active object'), \
                 ('ALL', 'ALL', 'All GP materials in Blender file')]
  
class GPCOLORMAP_OT_addMapping(bpy.types.Operator):
    bl_idname = "scene.add_mapping"
    bl_label = "Add mapping"
    bl_description = "Add mapping"

    mode: EnumProperty(items=mode_add_enum, name="Mode", default='PENDING')

    def execute(self, context):   
        gpcm = context.scene.gpcolormapping

        if self.mode == 'PENDING':
            if not gpcm.is_material_available(gpcm.pending_material):
                gpcm.pending_material = None
                return {'CANCELLED'}
            gpcm.accept_pending(context)            
            bpy.ops.ed.undo_push()
            return {'FINISHED'}

        mats = set()
        if self.mode == 'ACTIVE':

            if (context.active_object is None) or \
                (context.active_object.type != 'GPENCIL'):
                return {'CANCELLED'}

            mats = { mslot.material for mslot in context.active_object.material_slots \
                if gpcm.is_material_available(mslot.material) }

        elif self.mode == 'ALL':
            mats = set([m for m in bpy.data.materials \
                if gpcm.is_material_available(m) ])

        for mat in mats:
            if not gpcm.is_material_available(mat):
                continue
            mpg = gpcm.mappings.add()
            mpg.base_material = mat            

        bpy.ops.ed.undo_push()
        return {'FINISHED'}   
        
class GPCOLORMAP_OT_removeMapping(bpy.types.Operator):
    bl_idname = "scene.remove_mapping"
    bl_label = "Remove"
    bl_description = "Remove color mapping and associated modifiers"

    mapping_id: IntProperty(name="Mapping ID", default=-1)

    @classmethod
    def poll(cls, context):
        return not context.scene.gpcolormapping.is_empty()

    def execute(self, context):   
        gpcm = context.scene.gpcolormapping

        if self.mapping_id >= 0:
            gpcm.remove_mapping(context, self.mapping_id)
        else:
            gpcm.remove_all(context)

        return {'FINISHED'}   
        
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_confirm(self, event)

class GPCOLORMAP_OT_resetMappings(bpy.types.Operator):
    bl_idname = "scene.reset_mappings"
    bl_label = "Reset all"
    bl_description = "Reset all color mappings to their source colors"

    @classmethod
    def poll(cls, context):
        return not context.scene.gpcolormapping.is_empty()

    def execute(self, context):   
        gpcm = context.scene.gpcolormapping
        gpcm.reset(context)

        return {'FINISHED'}   

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_confirm(self, event)

class GPCOLORMAP_OT_cleanMappings(bpy.types.Operator):
    bl_idname = "scene.clean_mappings"
    bl_label = "Clear"
    bl_description = "Remove inactive mappings, and identity transformations"

    @classmethod
    def poll(cls, context):
        return not context.scene.gpcolormapping.is_empty()

    def execute(self, context):   
        gpcm = context.scene.gpcolormapping
        
        mappings_toremove = [ (ind,mpg.name) for ind,mpg in enumerate(gpcm.mappings) \
                            if ((not mpg.is_active) or (mpg.is_identity())) ]

        for ind,mpg in reversed(mappings_toremove):
            gpcm.remove_mapping(context, ind)

        return {'FINISHED'}   

class GPCOLORMAP_OT_refreshMappings(bpy.types.Operator):
    bl_idname = "scene.refresh_mappings"
    bl_label = "Refresh"
    bl_description = "Update mappings and modifiers accounting for new objects and outdated materials"

    @classmethod
    def poll(cls, context):
        return not context.scene.gpcolormapping.is_empty()

    def execute(self, context):   
        gpcm = context.scene.gpcolormapping
        gpcm.refresh(context)

        return {'FINISHED'}   

class GPCOLORMAP_OT_exportMappings(bpy.types.Operator, ExportHelper):
    bl_idname = "scene.export_mappings"
    bl_label = "Export"
    bl_description = "Exports a set of color mappings as JSON file" 
    
    filename_ext = ".json"
    filter_glob: bpy.props.StringProperty(
        default="*.json",
        options={'HIDDEN'},
        maxlen=255,  # Max internal buffer length, longer would be clamped.
    )

    @classmethod
    def poll(cls, context):
        return not context.scene.gpcolormapping.is_empty()

    def execute(self, context): 
        from . colormap_io import export_mappings_content
        data = export_mappings_content(context)

        with open(self.filepath, 'w') as outfile:
            json.dump(data, outfile, indent=4)
        
        self.report({'INFO'}, f"Mappings exported to {self.filepath}")
        return {'FINISHED'}

class GPCOLORMAP_OT_importMappings(bpy.types.Operator, ImportHelper):
    bl_idname = "scene.import_mappings"
    bl_label = "Import"
    bl_description = "Import a set of color mappings from a JSON file"  
    
    filename_ext = ".json"
    filter_glob: bpy.props.StringProperty(
        default="*.json",
        options={'HIDDEN'},
        maxlen=255,  # Max internal buffer length, longer would be clamped.
    )

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context): 

        ifl = open(self.filepath, 'r')
        data = json.load(ifl)
        ifl.close()

        from . colormap_io import import_mappings_content
        import_mappings_content(context, data)
    
        return {'FINISHED'}

classes = [GPCOLORMAP_OT_applyMapping, \
           GPCOLORMAP_OT_addMapping, \
           GPCOLORMAP_OT_removeMapping, \
           GPCOLORMAP_OT_resetMappings, \
           GPCOLORMAP_OT_cleanMappings, \
           GPCOLORMAP_OT_refreshMappings, \
           GPCOLORMAP_OT_exportMappings, \
           GPCOLORMAP_OT_importMappings]

def register():
    for cls in classes:
        bpy.utils.register_class(cls) 

def unregister():        
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)