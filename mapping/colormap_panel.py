import bpy
from bpy.props import *

class GPCOLORMAP_PT_MatTabInfoPanel(bpy.types.Panel):
    bl_label="Empty panel"
    bl_idname="GPCOLORMAP_PT_MatTabInfoPanel"
    bl_space_type = 'INFO'
    bl_region_type = 'WINDOW'
    is_popover=True
    bl_options= {'HIDE_HEADER'}

    def draw(self, context):
        layout = self.layout
        layout.label(text="Go to Material tab to change color")

class GPCOLORMAP_UL_MapperList(bpy.types.UIList):
    bl_idname="GPCOLORMAP_UL_MapperList"
 
    def draw_item(self, context, layout, data, item, icon, active_data,
                  active_propname, index):

        layout.ui_units_y = 1.1

        if item.base_material is None:
            col_name = layout.column()
            col_name.alert = True
            col_name.label(text=f"Material {item.name} not found")
            col_name.ui_units_x = 18

            col_trash = layout.column()
            rmp = col_trash.operator("scene.remove_mapping", icon="TRASH", text="", emboss=False)
            rmp.mapping_id = index            
            return

        show_fill = item.base_material.grease_pencil.show_fill
        show_stroke = item.base_material.grease_pencil.show_stroke

        col_group = layout.column()
        col_group.prop(item, "group", text="", emboss=False)
        col_group.enabled = item.is_active
        col_group.ui_units_x = 2

        col_name = layout.column()
        col_name.label(text=item.name)
        col_name.enabled = item.is_active
        col_name.ui_units_x = 18

        col_app = layout.column()
        app = col_app.operator("scene.apply_mapping", icon="CHECKMARK", text="", emboss=False)
        app.mapping_id = index
        col_app.enabled = item.is_active

        col_basemat = layout.column()
        row = col_basemat.row(align=True)         
        if show_stroke:
            row.prop_with_popover(item.base_material.grease_pencil, "color", text="", panel="GPCOLORMAP_PT_MatTabInfoPanel")
        else:
            row.label(text="")
        if show_fill:
            row.prop_with_popover(item.base_material.grease_pencil, "fill_color", text="", panel="GPCOLORMAP_PT_MatTabInfoPanel")
        else:
            row.label(text="")
        col_basemat.ui_units_x = 15

        col_activate = layout.column()
        if item.is_active:
            tpv_icon = 'RESTRICT_RENDER_OFF'
        else:
            tpv_icon = 'RESTRICT_RENDER_ON'
        col_activate.prop(item, "is_active", icon=tpv_icon, text="", emboss=False)

        col_link = layout.column()
        if not (show_fill and show_stroke):
            lkd_icon = 'NONE'
        elif item.is_linked:
            lkd_icon = 'LINKED'
        else:
            lkd_icon = 'UNLINKED'
        col_link.prop(item, "is_linked", icon=lkd_icon, text="", emboss=False)
        col_link.enabled = (item.is_active) \
            and (item.base_material.grease_pencil.show_fill \
            and item.base_material.grease_pencil.show_stroke)

        col_target = layout.column()
        row = col_target.row(align=True)
        if show_stroke:
            row.prop(item, "stroke_color", text="")
        if ((not item.is_linked) or (not show_stroke)) \
            and show_fill:
            row.prop(item, "fill_color", text="")
        col_target.enabled = item.is_active
        col_target.ui_units_x = 15

        col_trash = layout.column()
        rmp = col_trash.operator("scene.remove_mapping", icon="TRASH", text="", emboss=False)
        rmp.mapping_id = index

class GPCOLORMAP_PT_AddMappingPanel(bpy.types.Panel):
    bl_label="Add mapping"
    bl_idname="GPCOLORMAP_PT_AddMappingPanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Color Mapping"
    is_popover=True

    def draw(self, context):
        gpcm = context.scene.gpcolormapping

        layout = self.layout
        no_mat = gpcm.pending_material is None
        select_valid = gpcm.is_material_available(gpcm.pending_material)
        
        row = layout.row()
        if context.space_data.type == 'VIEW_3D':
            row.operator("scene.colormap_pick_material", text="", icon='EYEDROPPER', emboss=False)

        col_mat = row.column()
        col_mat.alignment = 'LEFT'
        if no_mat:
            col_mat.template_ID(gpcm, "pending_material", text="Material")
        else:
            col_mat.template_ID(gpcm, "pending_material")

        col_valid = row.column()
        col_valid.alignment = 'RIGHT'
        col_valid.enabled = select_valid
        col_valid.alert = (not no_mat) and (not select_valid)
        col_valid.ui_units_x = 1
        col_valid.operator("scene.add_mapping", text="OK").mode = 'PENDING'

        row = layout.row()

class GPCOLORMAP_MT_MappingPresetSubmenu(bpy.types.Menu):
    bl_idname = "GPCOLORMAP_MT_mapping_presets_submenu"
    bl_label = "Presets.."

    def draw(self, context):
        layout = self.layout

        layout.operator("scene.grayscale_color_mapping")

class GPCOLORMAP_MT_MappingMenu(bpy.types.Menu):
    bl_idname = "GPCOLORMAP_MT_mapping_menu"
    bl_label = "Mapping operations"

    def draw(self, context):
        layout = self.layout

        layout.operator("scene.clean_mappings")
        layout.operator("scene.reset_mappings", icon="RECOVER_LAST")
        layout.operator("scene.remove_mapping", icon="TRASH", text="Remove all mappings").mapping_id = -1
        layout.operator("scene.add_mapping", text="Materials in Active Object", icon="ADD").mode = 'ACTIVE'
        layout.operator("scene.add_mapping", text="All Materials", icon="ADD").mode = 'ALL'
        layout.menu("GPCOLORMAP_MT_mapping_presets_submenu")

class GPCOLORMAP_PT_Manager(bpy.types.Panel):
    bl_label="Grease Pencil Color Mapper"
    bl_idname="GPCOLORMAP_PT_Manager"
    bl_space_type="PROPERTIES"
    bl_region_type="WINDOW"
    bl_context="scene"

    def draw(self, context):
        layout = self.layout

        cmap = context.scene.gpcolormapping

        row = layout.row()
        col_render = row.column()
        if cmap.is_active:
            tpv_icon = 'RESTRICT_RENDER_OFF'
        else:
            tpv_icon = 'RESTRICT_RENDER_ON'
        col_render.enabled = not cmap.is_empty()
        col_render.prop(cmap, "is_active", text="", icon=tpv_icon, emboss=False)

        row.alignment = 'RIGHT'
        row.operator("scene.apply_mapping", icon="CHECKMARK", text="", emboss=False).mapping_id = -1
        row.operator("scene.refresh_mappings", icon="FILE_REFRESH", text="")
        row.popover("GPCOLORMAP_PT_AddMappingPanel", icon="ADD", text="")

        row.menu("GPCOLORMAP_MT_mapping_menu", text="")

        row = layout.row()
        row.template_list("GPCOLORMAP_UL_MapperList",'GP_ColorMapping', \
                        dataptr=cmap, propname="mappings", \
                        active_dataptr=cmap, active_propname="active_index", \
                        )

        row = layout.row()
        row.alignment = 'RIGHT'
        row.operator("scene.import_mappings", icon="IMPORT", text="")
        row.operator("scene.export_mappings", icon="EXPORT", text="")

classes = [GPCOLORMAP_PT_MatTabInfoPanel, \
           GPCOLORMAP_PT_AddMappingPanel , \
           GPCOLORMAP_UL_MapperList, \
           GPCOLORMAP_MT_MappingPresetSubmenu, \
           GPCOLORMAP_MT_MappingMenu, \
           GPCOLORMAP_PT_Manager]
def register():
    for cls in classes:
        bpy.utils.register_class(cls) 

def unregister():        
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)